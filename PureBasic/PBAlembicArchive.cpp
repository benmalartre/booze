#include "PBAlembicRegister.h"

EXPORT AlembicArchiveManager* ABC_CreateArchiveManager()
{
	AlembicArchiveManager* m = new AlembicArchiveManager();
	return m;
}

EXPORT void  ABC_DeleteArchiveManager(AlembicArchiveManager* manager)
{
	if(manager!=NULL){
		manager->DeleteAllArchives();
		delete manager;
	}
}

EXPORT long ABC_GetNumOpenArchives(AlembicArchiveManager* manager)
{
	return manager->GetNumOpenArchives();
}

EXPORT TCHAR* ABC_GetInfosFromArchive(AlembicIArchive* archive)
{
	std::string infos;

	if(archive)
	{
		infos = Alembic::AbcCoreAbstract::GetLibraryVersion ()+"|";
		std::string appName;
        std::string libraryVersionString;
        Alembic::Util::uint32_t libraryVersion;
        std::string whenWritten;
        std::string userDescription;
        GetArchiveInfo (*archive->GetArchive(),
                        appName,
                        libraryVersionString,
                        libraryVersion,
                        whenWritten,
                        userDescription);

		if (appName != "")
        {
            infos += "  file written by: " + appName+"|";
            infos += "  using Alembic : " + libraryVersionString+"|";
            infos += "  written on : " + whenWritten +"|";
            infos += "  user description : " + userDescription+"|";
        }
        else
        {
            infos += archive->GetName();
            infos += "|(file doesn't have any ArchiveInfo)|";
        }
	}

	int size = (int)infos.size()+1;
	SYS_FastStringCopy(&alembic_io_string, infos.c_str(), size);
	return &alembic_io_string;
}

EXPORT AlembicIArchive* ABC_OpenArchive(AlembicArchiveManager* manager,TCHAR* filename)
{
	std::string path(filename);
	AlembicIArchive* archive = manager->GetArchiveFromID(path);
	return archive;
}

EXPORT long ABC_GetNumObjectsInArchive(AlembicIArchive* archive)
{
	return archive->GetNumObjects();
}

EXPORT long ABC_GetNumTimeSamplingInArchive(AlembicIArchive* archive)
{
	return archive->GetNumTimeSamplings();
}