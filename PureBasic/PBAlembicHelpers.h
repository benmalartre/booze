#include "PBAlembicRegister.h"

EXPORT TCHAR* ABC_PassStringToPureBasic(std::string& str)
{
   int size = (int)str.size()+1;
   SYS_FastStringCopy(&alembic_io_string, str.c_str(), size);
   return &alembic_io_string;
}

EXPORT Alembic::Abc::chrono_t ABC_GetStartFrame(AlembicIArchive* archive)
{
	Alembic::Abc::IArchive* a = archive->GetArchive();
	Alembic::AbcCoreAbstract::TimeSamplingPtr sampler(a->getTimeSampling(0));
	return sampler->getSampleTime(0);
	
}

EXPORT Alembic::Abc::chrono_t ABC_GetEndFrame(AlembicIArchive* archive)
{
	Alembic::AbcCoreAbstract::TimeSamplingPtr sampler = archive->GetArchive()->getTimeSampling(0);
	Alembic::AbcCoreAbstract::index_t t = archive->GetArchive()->getMaxNumSamplesForTimeSamplingIndex(0);
	return sampler->getSampleTime(t);
}
